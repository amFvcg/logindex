export interface HistoricalData {
  period: string;
  value: number;
}
