import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { DataSelectorComponent } from './data-selector/data-selector.component';
import { HistoricalDataRawViewerComponent } from './historical-data-raw-viewer/historical-data-raw-viewer.component';
import { ChartComponent } from './chart/chart.component';

import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

import { NgxEchartsModule } from "ngx-echarts";

@NgModule({
  declarations: [
    AppComponent,
    DataSelectorComponent,
    HistoricalDataRawViewerComponent,
    ChartComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgxEchartsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
