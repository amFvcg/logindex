import { Injectable } from '@angular/core';
import { Observable, of } from "rxjs";
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { HistoricalData } from "./datamodel/historical_data";

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  private backend_address : string = '';

  constructor(
    private http: HttpClient
  ) { }

  getTradeTypes(country_code: string): Observable<string[]> {
    return this.http.get<string[]>(`${this.backend_address}/trade_types/${country_code}`)
  }

  getCountryCodes(): Observable<string []> {
    return this.http.get<string[]>(`${this.backend_address}/country_codes`);
  }

  getHistoricalData(country_code: string, trade_type: string): Observable<HistoricalData[]> {
    return this.http.get<HistoricalData[]>(`${this.backend_address}/historical_data/${country_code}/${trade_type}`);
  }

  getMovingAverage(country_code: string, trade_type: string): Observable<HistoricalData[]> {
    return this.http.get<HistoricalData[]>(`${this.backend_address}/moving_average/${country_code}/${trade_type}`);
  }
  getYoY(country_code: string, trade_type: string): Observable<HistoricalData[]> {
    return this.http.get<HistoricalData[]>(`${this.backend_address}/yoy/${country_code}/${trade_type}`);
  }

  getMoM(country_code: string, trade_type: string): Observable<HistoricalData[]> {
    return this.http.get<HistoricalData[]>(`${this.backend_address}/mom/${country_code}/${trade_type}`);

}
