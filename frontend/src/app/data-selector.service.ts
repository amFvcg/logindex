import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataSelectorService {

  private data_selector_source = new BehaviorSubject<string[]>([''])
  data_selector_sink = this.data_selector_source.asObservable();
  constructor() { }

  announceDataSelector(data: string[]){
    this.data_selector_source.next(data);
  }
}
