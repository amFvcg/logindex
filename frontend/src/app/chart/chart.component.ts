import { Component, OnInit, OnDestroy } from '@angular/core';
import { EChartOption } from 'echarts';
import { BackendService } from "../backend.service";
import {HistoricalData} from "../datamodel/historical_data";
import { DataSelectorService } from '../data-selector.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnDestroy, OnInit {

  monthly_chart_option: EChartOption;

  mom_chart_option: EChartOption;

  yoy_chart_option: EChartOption;

  chart_data = {
    historical_data : [],
    moving_average: [],
    mom: [],
    yoy: [],
    x_axis_data: []
  }

  data_selector_subscription: Subscription;

  constructor(private backend_service: BackendService, private data_selector_service: DataSelectorService) { 
    this.data_selector_subscription = data_selector_service.data_selector_sink.subscribe(
      data => { if (data.length == 2) this.drawChart(data[0], data[1]) }
    )
  }
  ngOnInit() {
    this.data_selector_subscription = this.data_selector_service.data_selector_sink.subscribe(
      data => { if (data.length == 2) this.drawChart(data[0], data[1]) }
    )
  }

  ngOnDestroy() {
    this.data_selector_subscription.unsubscribe();
  }

  drawChart(country_code: string, trade_type: string): void {
    this.backend_service.getHistoricalData(country_code, trade_type).
      subscribe( data => this.monthly_chart_option = this.prepareMonthlyChart(data, 'historical_data'));
    this.backend_service.getMovingAverage(country_code, trade_type).
      subscribe( data => this.monthly_chart_option = this.prepareMonthlyChart(data, 'moving_average'));
    this.backend_service.getMoM(country_code, trade_type).
      subscribe( data => this.mom_chart_option = this.prepareChart(data, 'mom'));
    this.backend_service.getYoY(country_code, trade_type).
      subscribe( data => this.yoy_chart_option = this.prepareChart(data, 'yoy'));
  }

  prepareMonthlyChart(dataset : HistoricalData[], data_type: string): EChartOption {
    var keys : string[] = []
    var values : number[] = []
    dataset.forEach((prop) => { keys.push(prop[0]); values.push(prop[1]);});
    this.chart_data[data_type] = values;
    console.log(this.chart_data)
    console.log(keys)
    return {
      xAxis: {
        type: 'category',
        data: keys
      },
      yAxis: {
        type: 'value'
      },
      dataZoom: [
        {id: 'dataZoomX',
        type: 'slider',
        xAxisIndex: [0],
        filterMode: 'filter'}
      ],
      legend: {
        data: [{name:'moving average'}, {name: 'historical_data'}]
      },
      series: [
        {
          name: 'moving average',
          data: this.chart_data['moving_average'],
          type: 'line'
        },{
          name: 'historical_data',
          data: this.chart_data['historical_data'],
          type: 'line'
        }
      ]
    }
  }

  prepareChart(dataset : HistoricalData[], data_type: string): EChartOption {
    var keys : string[] = []
    var values : number[] = []
    dataset.forEach((prop) => { keys.push(prop[0]); values.push(prop[1]);});
    this.chart_data[data_type] = values;
    var chartOption : EChartOption = {
      xAxis: {
        type: 'category',
        data: keys
      },
      yAxis: {
        type: 'value'
      },
      dataZoom: [
        {id: 'dataZoomX',
        type: 'slider',
        xAxisIndex: [0],
        filterMode: 'filter'}
      ],
      legend: {
        data: [{name: data_type}]
      },
      series: [
        {
          name: data_type,
          data: this.chart_data[data_type],
          type: 'line'
        }
      ]
    };
    return chartOption
  }

}
