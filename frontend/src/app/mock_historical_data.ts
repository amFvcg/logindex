import { HistoricalData } from "./datamodel/historical_data";

export const HISTORICAL_DATA: HistoricalData[] = [
  {period: "201801", value: 100},
  {period: "201802", value: 300},
  {period: "201803", value: 200},
  {period: "201804", value: 400},
  {period: "201805", value: 200},
  {period: "201806", value: 600},
  {period: "201807", value: 300},
  {period: "201808", value: 200},
  {period: "201809", value: 100},
  {period: "201810", value: 200},
  {period: "201811", value: 300},
  {period: "201812", value: 100},
];
