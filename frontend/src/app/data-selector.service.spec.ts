import { TestBed, inject } from '@angular/core/testing';

import { DataSelectorService } from './data-selector.service';

describe('DataSelectorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataSelectorService]
    });
  });

  it('should be created', inject([DataSelectorService], (service: DataSelectorService) => {
    expect(service).toBeTruthy();
  }));
});
