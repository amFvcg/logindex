import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricalDataRawViewerComponent } from './historical-data-raw-viewer.component';

describe('HistoricalDataRawViewerComponent', () => {
  let component: HistoricalDataRawViewerComponent;
  let fixture: ComponentFixture<HistoricalDataRawViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricalDataRawViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricalDataRawViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
