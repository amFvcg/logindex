import { Component, OnInit, OnDestroy } from '@angular/core';
import { HistoricalData } from "../datamodel/historical_data";
import { BackendService } from "../backend.service";
import { DataSelectorService } from "../data-selector.service";
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-historical-data-raw-viewer',
  templateUrl: './historical-data-raw-viewer.component.html',
  styleUrls: ['./historical-data-raw-viewer.component.css']
})
export class HistoricalDataRawViewerComponent implements OnDestroy {

  historical_data: HistoricalData[];
  moving_average: HistoricalData[];
  mom: HistoricalData[];
  yoy: HistoricalData[];

  data_selector_subscription : Subscription;

  constructor(private backend_service: BackendService, private data_selector_service: DataSelectorService) { 
    this.data_selector_subscription = this.data_selector_service.data_selector_sink.subscribe( 
      data => {
        if (data.length == 2)
        {
          this.getHistoricalData(data[0], data[1]);
          this.getMovingAverage(data[0], data[1]);
          this.getMoM(data[0], data[1]);
          this.getYoY(data[0], data[1]);
        }
      });
  }

  ngOnDestroy() {
    this.data_selector_subscription.unsubscribe();
  }

  getHistoricalData(country_code: string, trade_type: string): void {
    this.backend_service.getHistoricalData(country_code, trade_type)
      .subscribe(historical_data => this.historical_data = historical_data );
  }

  getMovingAverage(country_code: string, trade_type: string): void {
    this.backend_service.getMovingAverage(country_code, trade_type)
      .subscribe(data => this.moving_average = data );
  }

  getMoM(country_code: string, trade_type: string): void {
    this.backend_service.getMoM(country_code, trade_type)
      .subscribe(data => this.mom = data );
  }

  getYoY(country_code: string, trade_type: string): void {
    this.backend_service.getYoY(country_code, trade_type)
      .subscribe(data => this.yoy = data );
  }

}
