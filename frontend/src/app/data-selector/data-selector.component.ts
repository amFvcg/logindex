import { Component, OnInit } from '@angular/core';
import { BackendService } from "../backend.service";
import { DataSelectorService } from "../data-selector.service";

@Component({
  selector: 'app-data-selector',
  templateUrl: './data-selector.component.html',
  styleUrls: ['./data-selector.component.css']
})
export class DataSelectorComponent implements OnInit {

  country_codes : string[];
  current_code: string;

  trade_types : string[];
  current_trade_type : string;

  constructor(private backendService: BackendService, private data_selector_service: DataSelectorService) { }

  ngOnInit() {
    this.getCountryCodes();
  }

  getCountryCodes(): void {
    this.backendService.getCountryCodes()
      .subscribe(country_codes => this.country_codes = country_codes );
  }

  getTradeTypes(country_code: string): void {
    this.backendService.getTradeTypes(country_code)
      .subscribe(trade_types => this.trade_types = trade_types );
    this.current_trade_type = "";
  }

  announce(): void {
    if (this.current_code === undefined || this.current_trade_type === undefined)
    {
      console.log("Undefined values, skipping")
      return;
    }
    this.data_selector_service.announceDataSelector([this.current_code, this.current_trade_type]);
  }

}
