from decimal import Decimal
from enum import Enum
from dataclasses import dataclass


class TradeType(Enum):
    Import = 'I'
    Export = 'E'

@dataclass
class Month:
    year: int
    month: int


@dataclass
class TradeData:
    period: Month
    declarant_iso: str
    partner_iso: str
    trade_type: TradeType
    value_eur: Decimal

class Meta:
    class TradeData(Enum):
        t_name = 'trade_data'
        f_period = 'period'
        f_declarant_iso = 'declarant_iso'
        f_partner_iso = 'partner_iso'
        f_trade_type = 'trade_type'
        f_value_eur = 'value_eur'
