import sqlite3
from backend.datamodel import Meta

#TODO: wrap it up, so that if 'connect' fails - we'd handle it somehow
def create_connection(address: str) -> sqlite3.Connection:
    return sqlite3.connect(
            address,
            detect_types=sqlite3.PARSE_DECLTYPES)


def create_init_db_script():
    # TODO: add type specification to Meta.TradeData so it can be used in table
    # generation
    # Or use SQLAlchemy ;)
    script = f"""
DROP TABLE IF EXISTS {Meta.TradeData.t_name.value};

CREATE TABLE {Meta.TradeData.t_name.value} (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    {Meta.TradeData.f_period.value} TEXT NOT NULL,
    {Meta.TradeData.f_declarant_iso.value} TEXT NOT NULL,
    {Meta.TradeData.f_partner_iso.value} TEXT NOT NULL,
    {Meta.TradeData.f_trade_type.value} TEXT NOT NULL,
    {Meta.TradeData.f_value_eur.value} NUMERIC NOT NULL
);
"""
    return script


def get_trade_types(db: sqlite3.Connection, country_code: str = None):
    query = f"SELECT DISTINCT({Meta.TradeData.f_trade_type.value}) " + \
            f"FROM {Meta.TradeData.t_name.value}"
    if country_code is not None:
        query += f" WHERE {Meta.TradeData.f_declarant_iso.value}='{country_code}'"
    print(query)
    result = db.execute(query).fetchall()
    return [i[0] for i in result]

def get_countries(db: sqlite3.Connection):
    query = f"SELECT DISTINCT({Meta.TradeData.f_declarant_iso.value}) " + \
            f"FROM {Meta.TradeData.t_name.value}"
    result = db.execute(query).fetchall()
    return [i[0] for i in result]

def get_historical_data(db: sqlite3.Connection, country_code : str, trade_type : str):
    query = f"SELECT {Meta.TradeData.f_period.value}, SUM({Meta.TradeData.f_value_eur.value}) " + \
            f"FROM {Meta.TradeData.t_name.value} " + \
            f"WHERE {Meta.TradeData.f_declarant_iso.value}='{country_code}' " + \
                f"AND {Meta.TradeData.f_trade_type.value}='{trade_type}' " + \
            f"GROUP BY {Meta.TradeData.f_period.value} " + \
            f"ORDER BY {Meta.TradeData.f_period.value}"
    result = db.execute(query).fetchall()
    # TODO: we could do double checking of data (first is done in data loader)
    return result

