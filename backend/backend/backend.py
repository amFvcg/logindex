import click
import json

from flask import Flask, Response, g, current_app
from flask_cors import CORS
from flask.cli import FlaskGroup
from functools import wraps
from itertools import groupby

import backend.db as db

def get_db():
    if 'db' not in g:
        # TODO: add loading configuration from file/etc
        g.db = db.create_connection(
            current_app.config.get('DATABASE', '/tmp/database')
        )
    return g.db


def get_moving_average(results: list, rolling_base : int = 12 ):
    rolling_base = min(rolling_base, len(results))
    keys = [i[0] for i in results]
    results = [i[1] for i in results]

    r = []
    m = results[0]
    r.append((keys[0], m))
    for i in range(1, len(results)):
        div = i+1
        if i >= rolling_base:
            div = rolling_base
            m -= results[i-rolling_base]/rolling_base
            m += results[i]/div
        else:
            m = m + (results[i]-m)/div
        r.append((keys[i], m))
    return r



def get_mom(results: list):
    keys = [i[0] for i in results]
    results = [i[1] for i in results]
    results.insert(0, results[0])
    return list(zip(keys, [(results[i]-results[i-1])/results[i-1] * 100 for i in range(1, len(results))]))

def get_yoy(results: list):
    results = [(year, sum([month_entry[1] for month_entry in group_obj]))
               for year, group_obj in groupby(results, key=lambda x: x[0][:4])]
    keys = [i[0] for i in results]
    results = [i[1] for i in results]
    results.insert(0, results[0])
    return list(zip(keys, [(results[i]-results[i-1])/results[i-1] * 100 for i in range(1, len(results))]))


def jsonify(func):
    @wraps(func)
    def create_response(*args, **kwargs):
        result = None
        status = 200
        # TODO: Proper error handling, with named Exceptions
        try:
            result = json.dumps(func(*args, **kwargs))
        except:
            status = 500
        return Response(result, status=status, mimetype='application/json')
    return create_response


def create_app():
    app = Flask (__name__)
    CORS(app)

    @app.route('/trade_types/<country_code>')
    @jsonify
    def trade_types_per_country_code(country_code):
        return db.get_trade_types(get_db(), country_code)

    @app.route('/trade_types')
    @jsonify
    def trade_types():
        return db.get_trade_types(get_db())

    @app.route('/country_codes')
    @jsonify
    def countries():
        return db.get_countries(get_db())

    @app.route('/historical_data/<country_code>/<trade_type>')
    @jsonify
    def historical_data(country_code, trade_type):
        return db.get_historical_data(get_db(), country_code, trade_type)

    @app.route('/moving_average/<country_code>/<trade_type>')
    @jsonify
    def rolling_average(country_code, trade_type):
        return get_moving_average(
            db.get_historical_data(get_db(), country_code, trade_type))

    @app.route('/mom/<country_code>/<trade_type>')
    @jsonify
    def mom(country_code, trade_type):
        return get_mom(
            db.get_historical_data(get_db(), country_code, trade_type))

    @app.route('/yoy/<country_code>/<trade_type>')
    @jsonify
    def yoy(country_code, trade_type):
        return get_yoy(
            db.get_historical_data(get_db(), country_code, trade_type))


    return app

@click.group(cls=FlaskGroup, create_app=create_app)
def cli():
    pass

if __name__ == "__main__":
    cli()
