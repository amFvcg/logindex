import os
import signal

def before_scenario(context, scenario):
    context.processes = []


def after_scenario(context, scenario):
    for proc in context.processes:
        pgrp = os.getpgid(proc.pid)
        os.killpg(pgrp, signal.SIGKILL)
        out, err = proc.communicate()
