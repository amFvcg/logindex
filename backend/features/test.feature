Feature: abc
    Scenario: ab
        Given sqlite3 is setup in file /tmp/database
        And below csv is loaded to /tmp/database
        """
        PERIOD,DECLARANT_ISO,PARTNER_ISO,TRADE_TYPE,VALUE_EUR
        201801,PL,UK,I,100.00
        201801,PL,UK,E,25.00
        201801,PL,DE,E,25.00
        201802,PL,UK,I,100.00
        201802,PL,UK,E,50.00
        201803,PL,UK,I,100.00
        201803,PL,UK,E,50.00
        201804,PL,UK,I,100.00
        201804,PL,UK,E,50.00
        201805,PL,UK,I,100.00
        201805,PL,UK,E,50.00
        201806,PL,UK,I,100.00
        201806,PL,UK,E,50.00
        201807,PL,UK,I,100.00
        201807,PL,UK,E,50.00
        201808,PL,UK,I,100.00
        201808,PL,UK,E,50.00
        201809,PL,UK,I,100.00
        201809,PL,UK,E,50.00
        201810,PL,UK,I,100.00
        201810,PL,UK,E,50.00
        201811,PL,UK,I,100.00
        201811,PL,UK,E,50.00
        201812,PL,UK,I,100.00
        201812,PL,UK,E,50.00
        201901,PL,UK,I,200.00
        201901,PL,UK,E,100.00
        201902,PL,UK,I,200.00
        201902,PL,UK,E,100.00
        201903,PL,UK,I,200.00
        201903,PL,UK,E,100.00
        201904,PL,UK,I,200.00
        201904,PL,UK,E,100.00
        201905,PL,UK,I,200.00
        201905,PL,UK,E,100.00
        201906,PL,UK,I,200.00
        201906,PL,UK,E,100.00
        201907,PL,UK,I,200.00
        201907,PL,UK,E,100.00
        201908,PL,UK,I,200.00
        201908,PL,UK,E,100.00
        201909,PL,UK,I,200.00
        201909,PL,UK,E,100.00
        201910,PL,UK,I,200.00
        201910,PL,UK,E,100.00
        201911,PL,UK,I,200.00
        201911,PL,UK,E,100.00
        201912,PL,UK,I,200.00
        201912,PL,UK,E,100.00
        """
        And backend server is up and running 
        When server is triggered with GET request on uri /trade_types
        Then the response is as below
        """
        ["E", "I"]
        """
        When server is triggered with GET request on uri /country_codes
        Then the response is as below
        """
        ["PL"]
        """
        When server is triggered with GET request on uri /yoy/PL/E
        Then the response is as below
        """
        [["2018", 0], ["2019", 100]]
        """
        When server is triggered with GET request on uri /mom/PL/E
        Then the response is as below
        """
        [
        ["201801", 0], ["201802", 0], ["201803", 0], ["201804", 0], ["201805", 0], ["201806", 0], ["201807", 0], ["201808", 0], ["201809", 0], ["201810", 0], ["201811", 0], ["201812", 0], 
        ["201901", 100], ["201902", 0], ["201903", 0], ["201904", 0], ["201905", 0], ["201906", 0], ["201907", 0], ["201908", 0], ["201909", 0], ["201910", 0], ["201911", 0], ["201912", 0]
        ]
        """
        #        When server is triggered with GET request on uri /moving_average/PL/E
        #        Then the response is as below
        #        """
        #        [
        #        ["25018501", 50], ["25018502", 50], ["25018503", 50], ["25018504", 50], ["25018505", 50], ["25018506", 50], ["25018507", 50], ["25018508", 50], ["25018509", 50], ["25018150", 50], ["2501811", 50], ["2501812", 50], 
        #        ["201901", 54.17], ["201902", 0], ["201903", 0], ["201904", 0], ["201905", 0], ["201906", 0], ["201907", 0], ["201908", 0], ["201909", 0], ["201910", 0], ["201911", 0], ["201912", 100]
        #        ]
        #        """
