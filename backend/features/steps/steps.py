import json
import requests
import sqlite3

import shlex
import subprocess

from behave import given, when, then

def run_command(cmd: str) -> subprocess.Popen:
    return subprocess.Popen(shlex.split(cmd), start_new_session=True)

@given('sqlite3 is setup in file {fname}')
def setup_sqlite3(context, fname):
    context.db = sqlite3.connect(fname)

@given('below csv is loaded to {fname}')
def csv_load(context, fname):
    import tempfile
    csv_file = tempfile.NamedTemporaryFile(delete=False)
    csv_file.write(context.text.encode())
    csv_file.close()

    out, err = run_command(f"logindex-data --init --sink_db {fname} --source_file {csv_file.name}").communicate()
    print(out, err)

@given('backend server is up and running')
def run_backend(context):
    context.processes.append(run_command(f"logindex-backend run"))
    import time
    time.sleep(3)

@when('server is triggered with GET request on uri /{uri}')
def server_triggered(context, uri):
    addr = f'http://localhost:5000/{uri}'
    result = requests.get(addr)
    print(result)
    print(result.text)
    assert result.ok
    context.result = result.json()

@then('the response is as below')
def check_response(context):
    assert sorted(context.result) == sorted(json.loads(context.text))
