from setuptools import find_packages, setup

setup(
    name='backend',
    version='0.1.0',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask',
        'flask_cors',
        'click'
    ],
    setup_requires=["pytest-runner"],
    tests_require=["pytest", "behave", "requests"],
    entry_points={
        'console_scripts': [
            'logindex-backend = backend.backend:cli',
            'logindex-data = loader.loader:main'
        ]
    }

)
