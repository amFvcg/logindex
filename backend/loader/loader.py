import click
import csv
import sqlite3
from decimal import Decimal
import decimal
from enum import Enum

from backend.datamodel import TradeData, TradeType, Month

class InvalidEntry(Exception):
    pass


class TradeDataCSVSpec:
    class Fields(Enum):
        PERIOD = 'PERIOD'
        DECLARANT_ISO = 'DECLARANT_ISO'
        PARTNER_ISO = 'PARTNER_ISO'
        TRADE_TYPE = 'TRADE_TYPE'
        VALUE_EUR = 'VALUE_EUR'
    sorted_expected_keys = sorted((i.value for i in Fields))


def to_trade_data(entry: dict):
    if not sorted(entry.keys()) == TradeDataCSVSpec.sorted_expected_keys:
        raise InvalidEntry('Invalid fields')

    period = entry.get(TradeDataCSVSpec.Fields.PERIOD.value, '')
    if len(period) != 6:
        raise InvalidEntry(f"{TradeDataCSVSpec.Fields.PERIOD} - invalid")
    try:
        year = int(period[:4])
        month = int(period[4:])
    except ValueError:
        raise InvalidEntry(f"{TradeDataCSVSpec.Fields.PERIOD} - couldn't parse")
    if year < 1 or month < 1 or month > 12:
        raise InvalidEntry('{TradeDataCSVSpec.Fields.PERIOD} - data out of scale - negative year/month out of bound')

    try:
        value = Decimal(entry.get(TradeDataCSVSpec.Fields.VALUE_EUR.value))
    except decimal.InvalidOperation:
        raise InvalidEntry(f"{TradeDataCSVSpec.Fields.VALUE_EUR} - cannot parse to Decimal")

    #TODO: country code checks could be added here
    declarant_iso = entry.get(TradeDataCSVSpec.Fields.DECLARANT_ISO.value, '')
    partner_iso = entry.get(TradeDataCSVSpec.Fields.PARTNER_ISO.value, '')

    try:
        trade_type = TradeType(entry.get(TradeDataCSVSpec.Fields.TRADE_TYPE.value))
    except ValueError:
        raise InvalidEntry(f"{TradeDataCSVSpec.Fields.TRADE_TYPE} - invalid")


    return TradeData(period=Month(year, month),
                      declarant_iso=declarant_iso,
                      partner_iso=partner_iso,
                      trade_type=trade_type,
                      value_eur=value)


def upload(db_address: str, data: list, init_db: bool):

    # TODO: handle errors
    conn = sqlite3.connect(db_address)
    cur = conn.cursor()
    if init_db:
        from backend.db import create_init_db_script
        cur.executescript(create_init_db_script())
    for entry in data:
        #TODO: clean this up
        cur.execute(f"insert into trade_data values (NULL, '{entry.period.year}{entry.period.month:0>2}', '{entry.declarant_iso}', '{entry.partner_iso}', '{entry.trade_type.value}', {entry.value_eur})")
    cur.close()
    conn.commit()
    conn.close()

def parse(filename: str, errors):
    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            try:
                yield to_trade_data(row)
            except InvalidEntry as e:
                errors.append((row, e))


# Sample extensions can consits of:
# - accepting --source as an uri, with dispatch via scheme (http/ftp/file/etc);
# - parsing --source file extension to deduce format
# - --sink can make use of uri scheme as well, to allow for ex. file storage, or
#   sending via http;

@click.command()
@click.option('--source_file', help='CSV file from which data should be read')
@click.option('--sink_db', help='DB addres to which data should be stored')
@click.option('--init', is_flag=True, default=False, help='Calls db init scripts')
def main(source_file: str, sink_db: str, init: bool):
    errors = []
    entries = parse(source_file, errors)
    uploaded_count = upload(sink_db, entries, init)
    print('\n'.join((str(err) for err in errors)))

if __name__ == "__main__":
    main()
