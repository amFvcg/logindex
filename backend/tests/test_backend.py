import unittest


class TestBusinessFunctions(unittest.TestCase):
    def test_moving_average_for_14_months(self):
        from backend.backend import get_moving_average
        dataset = [
            ("201801", 100),
            ("201802", 200),
            ("201803", 100),
            ("201804", 200),
            ("201805", 100),
            ("201806", 200),
            ("201807", 100),
            ("201808", 200),
            ("201809", 100),
            ("201810", 200),
            ("201811", 100),
            ("201812", 200),
            ("201901", 400),
            ("201902", 200),
        ]
        values = [i[1] for i in dataset]
        result = get_moving_average(dataset)
        expected = [
            ("201801", sum(values[:1])/1),
            ("201802", sum(values[:2])/2),
            ("201803", sum(values[:3])/3),
            ("201804", sum(values[:4])/4),
            ("201805", sum(values[:5])/5),
            ("201806", sum(values[:6])/6),
            ("201807", sum(values[:7])/7),
            ("201808", sum(values[:8])/8),
            ("201809", sum(values[:9])/9),
            ("201810", sum(values[:10])/10),
            ("201811", sum(values[:11])/11),
            ("201812", sum(values[:12])/12),
            ("201901", sum(values[1:13])/12),
            ("201902", sum(values[2:14])/12),
        ]
        self.assertEqual(len(result), len(expected))
        for i, (month, value) in enumerate(result):
            self.assertEqual(month, expected[i][0])
            self.assertAlmostEqual(value, expected[i][1], places=2)

    def test_moving_average_for_4_months(self):
        from backend.backend import get_moving_average
        dataset = [
            ("201801", 100),
            ("201802", 200),
            ("201803", 100),
            ("201804", 200),
        ]
        values = [i[1] for i in dataset]
        result = get_moving_average(dataset)
        expected = [
            ("201801", sum(values[:1])/1),
            ("201802", sum(values[:2])/2),
            ("201803", sum(values[:3])/3),
            ("201804", sum(values[:4])/4),
        ]
        self.assertEqual(len(result), len(expected))
        for i, (month, value) in enumerate(result):
            self.assertEqual(month, expected[i][0])
            self.assertAlmostEqual(value, expected[i][1], places=2)

    def test_moving_average_should_fill_in_holes_in_months(self):
        pass

    def test_mom(self):
        from backend.backend import get_mom
        dataset = [
            ("201801", 100),
            ("201802", 200),
            ("201803", 100),
            ("201804", 200),
        ]
        expected = [
            ("201801", 0),
            ("201802", 100),
            ("201803", -50),
            ("201804", 100),
        ]
        result = get_mom(dataset)
        self.assertEqual(len(result), len(expected))
        for i, (month, value) in enumerate(result):
            self.assertEqual(month, expected[i][0])
            self.assertAlmostEqual(value, expected[i][1], places=2)

    def test_yoy(self):
        from backend.backend import get_yoy
        dataset = [
            ("201801", 100),
            ("201802", 100),
            ("201903", 200),
            ("201904", 200),
        ]
        expected = [
            ("2018", 0),
            ("2019", 100),
        ]
        result = get_yoy(dataset)
        self.assertEqual(len(result), len(expected))
        for i, (year, value) in enumerate(result):
            self.assertEqual(year, expected[i][0])
            self.assertAlmostEqual(value, expected[i][1], places=2)
