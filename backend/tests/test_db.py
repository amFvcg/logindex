import unittest
import sqlite3


class TestDBBusinessFunctions(unittest.TestCase):
    def setUp(self):
        init_entries = [
            (None, "201801", "PL", "DE", "I", 123.50),
            (None, "201801", "PL", "UK", "I", 123.50),
            (None, "201802", "UK", "DE", "E", 200.11),
            (None, "201802", "PL", "UK", "I", 200.00),
            (None, "201803", "PL", "UK", "I", 300.00),
            (None, "201804", "PL", "UK", "I", 400.00),
            (None, "201805", "PL", "UK", "I", 500.00),
            (None, "201806", "PL", "UK", "I", 600.00),
            (None, "201807", "PL", "UK", "I", 500.00),
            (None, "201808", "PL", "UK", "I", 600.00),
            (None, "201809", "PL", "UK", "I", 700.00),
            (None, "201810", "PL", "UK", "I", 500.00),
            (None, "201811", "PL", "UK", "I", 400.00),
            (None, "201812", "PL", "UK", "I", 300.00),
            (None, "201901", "PL", "UK", "E", 200.00),
            (None, "201902", "PL", "UK", "I", 100.00),
        ]


        from backend.db import create_init_db_script
        self.db = sqlite3.connect(':memory:')
        self.db.executescript(create_init_db_script())
        self.db.executemany('INSERT INTO trade_data VALUES (?,?,?,?,?,?)',
                            init_entries)

    def tearDown(self):
        self.db.close()

    def test_get_trade_types_per_country_should_return_one_trade_type(self):
        from backend.db import get_trade_types
        result = get_trade_types(self.db, 'UK')
        self.assertListEqual(sorted(result), sorted(['E']))

    def test_get_trade_types_should_return_two_trade_types(self):
        from backend.db import get_trade_types
        result = get_trade_types(self.db)
        self.assertListEqual(sorted(result), sorted(['E', 'I']))

    def test_get_countries_should_return_two_countries(self):
        from backend.db import get_countries
        result = get_countries(self.db)
        self.assertListEqual(sorted(result), sorted(['PL', 'UK']))

    def test_get_historical_data_should_return(self):
        from backend.db import get_historical_data
        result = get_historical_data(self.db, 'PL', 'I')
        expected = [
            ("201801", 247.00),
            ("201802", 200.00),
            ("201803", 300.00),
            ("201804", 400.00),
            ("201805", 500.00),
            ("201806", 600.00),
            ("201807", 500.00),
            ("201808", 600.00),
            ("201809", 700.00),
            ("201810", 500.00),
            ("201811", 400.00),
            ("201812", 300.00),
            ("201902", 100.00),
        ]
        self.assertListEqual(result, expected)
