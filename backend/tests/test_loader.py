
import unittest
from decimal import Decimal


class TestToTradeDataConversion(unittest.TestCase):
    def test_proper_entry(self):
        from loader.loader import to_trade_data
        from backend.datamodel import TradeData, TradeType, Month
        dict_trade = {
            'PERIOD': '201801',
            'DECLARANT_ISO': 'PL',
            'PARTNER_ISO': 'DE',
            'TRADE_TYPE': 'E',
            'VALUE_EUR': '100'
        }
        expected = TradeData(
            period=Month(2018, 1),
            declarant_iso='PL',
            partner_iso='DE',
            trade_type=TradeType.Export,
            value_eur=Decimal(100)
        )
        result = to_trade_data(dict_trade)
        self.assertEqual(expected, result)

    def test_invalid_field_count(self):
        from loader.loader import to_trade_data, InvalidEntry
        from backend.datamodel import TradeData, TradeType, Month
        dict_trade = {
            'PERIOD': '201801',
            'DECLARANT_ISO': 'PL',
            'PARTNER_ISO': 'DE',
            'TRADE_TYPE': 'E',
        }
        self.assertRaises(InvalidEntry, to_trade_data, dict_trade)

    def test_invalid_period_with_longer_field(self):
        from loader.loader import to_trade_data, InvalidEntry
        from backend.datamodel import TradeData, TradeType, Month
        dict_trade = {
            'PERIOD': '2018010',
            'DECLARANT_ISO': 'PL',
            'PARTNER_ISO': 'DE',
            'TRADE_TYPE': 'E',
            'VALUE_EUR': '100'
        }
        self.assertRaises(InvalidEntry, to_trade_data, dict_trade)

    def test_invalid_period_with_month_out_of_scope(self):
        from loader.loader import to_trade_data, InvalidEntry
        from backend.datamodel import TradeData, TradeType, Month
        dict_trade = {
            'PERIOD': '201813',
            'DECLARANT_ISO': 'PL',
            'PARTNER_ISO': 'DE',
            'TRADE_TYPE': 'E',
            'VALUE_EUR': '100'
        }
        self.assertRaises(InvalidEntry, to_trade_data, dict_trade)

    def test_invalid_value_eur_with_nonparsable_value(self):
        from loader.loader import to_trade_data, InvalidEntry
        from backend.datamodel import TradeData, TradeType, Month
        dict_trade = {
            'PERIOD': '201811',
            'DECLARANT_ISO': 'PL',
            'PARTNER_ISO': 'DE',
            'TRADE_TYPE': 'E',
            'VALUE_EUR': 'iii'
        }
        self.assertRaises(InvalidEntry, to_trade_data, dict_trade)
