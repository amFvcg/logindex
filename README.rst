Interview webapp
================

Application consists of two parts:

* frontend - written in Angular6;

* backend - written in python3.7; with backend integration tests using behave;

Frontend is wrapped in docker container and serves application with nginx.

Backend is producing two entrypoint scripts:

* logindex-data - responsible for dbinit and loading data to db;

* logindex-backend - flask based application;

Backend is also wrapped in docker container but serves app with dev-state werkzeug.

To list available REST endpoints issue (in backend docker or after installing backend application with `python3 setup.py install`)::

  logindex-backend routes

Prerequisites
-------------

docker, docker-compose, nodejs, angular-cli

Installation
------------
Before performing `docker-compose up` exec::

  cd frontend
  ng build --prod --base-href /frontend/ --deploy-url /frontend/
  

Important notes
---------------
This is by no means a prod-ready application. It has many flaws, to mention only some:

* lack of configuration support;

* lack of caching mechanism (for both sides, backend and frontend; depends on requests profile);

* lack of optimisations like:

  - precalculation of mean/mom/yoy during data loading (no need to calculate it every time the request comes);

  - protocol optimisations - instead of returning tuples of [period, value] it might be better to return two lists: one with periods the other with values (this split is taking place in frontend);

* lack of proper styling in frontend;

* lack of proper flask app serving (gunicorn, uwsgi, etc);

* lack of proper models in frontend;

* lack of proper sql handling in data loader;

* lack of tests for frontend;

* there's a hack for resolving backend address which incorporates proxy_pass in nginx.conf;

* so on, so on...
